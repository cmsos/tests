#!/bin/bash

# print a trace of commands
set -x

# start xaad and put into background
/usr/bin/scl enable devtoolset-8 -- /opt/xdaq/bin/xdaq.exe -p 9950 -e /opt/xdaq/share/test2/profile/xaad.profile -z test2

# start jobcontrol and put into background
#/usr/bin/scl enable devtoolset-8 -- /opt/xdaq/bin/xdaq.exe -p 9999 -e /opt/xdaq/share/test2/profile/jobcontrol.profile -z test2 &

#/opt/xdaq/bin/xdaq.exe &

