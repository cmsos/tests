#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "test/monitorableapp/version.h"

GETPACKAGEINFO(testmonitorableapp)

void testmonitorableapp::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string>> testmonitorableapp::getPackageDependencies()
{
	std::set<std::string, std::less<std::string>> dependencies;

	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xdaq);

	return dependencies;
}

