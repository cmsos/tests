#ifndef _test_monitorableapp_Monitorable_h_
#define _test_monitorableapp_Monitorable_h_

#include "xdaq/Application.h"
#include "xdata/UnsignedInteger64.h"

namespace test {
	namespace monitorableapp {
		class Monitorable: public xdaq::Application, xdata::ActionListener
		{
			public:
				XDAQ_INSTANTIATOR();
				Monitorable(xdaq::ApplicationStub* s);
				void actionPerformed(xdata::Event& e);
			private:
				xdata::UnsignedInteger64 uint64Field_; 
		};
	}
}

#endif

