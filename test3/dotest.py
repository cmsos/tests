import sys
import os
import http.client
import json

elasticUrl = "elasticsearch1:9200"
connection = http.client.HTTPConnection(elasticUrl)
headers = {"Content-type": "application/json"}
indexname = "cmsos-data-test2-master-jobcontrol-flash"
url = indexname + "/_search?pretty"
b = { "query": { "match_all": {} }, "size": 1, "sort": [ { "creationtime_": { "order": "desc" } } ] }
body = json.dumps(b)

print("elasticUrl: " + elasticUrl)
print("url: " + url)
print("body: " + body)
print("headers: " + json.dumps(headers))

connection.request("GET", url, body, headers)
response = connection.getresponse()
result = response.read().decode()
print("Result:")
print(result)

jsonresult = json.loads(result)

home = json.dumps(jsonresult["hits"]["hits"][0]["_source"]["home"])
home = home.replace('"', '')

if home == "/opt/xdaq":
	print("Test passed")
else:
	print("Test failed")
	sys.exit(1)
