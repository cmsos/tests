#!/usr/bin/env sh

docker_compose_cmd="docker-compose -f test1/docker-compose.yml"

#${docker_compose_cmd} build
# Run service containers, except for test container
${docker_compose_cmd} up -d --no-deps elasticsearch1 elasticsearch2 jobcontrol localbus elastic #xaad slimbus slash2g

# Run the tests
echo "--------- output from test2 ----------"
${docker_compose_cmd} run test2

# keep the exit code from tests while we clean up the containers
exit_code=$?

#echo "--------- xaad logs ------------------"
#${docker_compose_cmd} logs xaad
#echo "--------- slimbus logs ------------------"
#${docker_compose_cmd} logs slimbus
##echo "--------- slash2g logs ------------------"
#${docker_compose_cmd} logs slash2g
echo "--------- elasticsearch1 logs ---------"
${docker_compose_cmd} logs elasticsearch1
echo "--------- elasticsearch2 logs --------"
${docker_compose_cmd} logs elasticsearch2
echo "--------- jobcontrol logs ------------------"
${docker_compose_cmd} logs jobcontrol
echo "--------- localbus logs ------------------"
${docker_compose_cmd} logs localbus
echo "--------- elastic logs ------------------"
${docker_compose_cmd} logs elastic
echo "--------- test2 logs -----------------"
${docker_compose_cmd} logs test2
echo "--------------------------------------"

# Clean up
${docker_compose_cmd} down

# return the original result of the test - for GitLab CI
exit ${exit_code}
