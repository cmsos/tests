#include "test/monitorableapp/Monitorable.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/ItemGroupEvent.h"

XDAQ_INSTANTIATOR_IMPL(test::monitorableapp::Monitorable)

test::monitorableapp::Monitorable::Monitorable(xdaq::ApplicationStub* s): xdaq::Application(s)
{
	//Create an infospace
	toolbox::net::URN monitorable = this->createQualifiedInfoSpace("testInfospace");
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(monitorable.toString());

	//Publish uint64Field in monitorable infospace
	is->fireItemAvailable("uint64Field", &uint64Field_);
	//Attach listener to uint64Field_ to detect retrieval event
	is->addGroupRetrieveListener(this);

	uint64Field_ = 1;

	LOG4CPLUS_INFO(this->getApplicationLogger(), "From test::monitorableapp::Monitorable::Monitorable"); 
}

void test::monitorableapp::Monitorable::actionPerformed(xdata::Event& e)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "From test::monitorableapp::Monitorable::actionPerformed");
	if ( e.type() == "urn:xdata-event:ItemGroupRetrieveEvent" )
	{
		xdata::ItemGroupRetrieveEvent& event = dynamic_cast<xdata::ItemGroupRetrieveEvent&>(e);
		if ( event.infoSpace()->name().find("urn:testInfospace") != std::string::npos )
		{
			uint64Field_ = 18446744073709551615U;
			LOG4CPLUS_INFO(this->getApplicationLogger(), "From test::monitorableapp::Monitorable::actionPerformed: uint64Field_ was assigned");
		}
	}
}

