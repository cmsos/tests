#ifndef _test_monitorableapp_version_h_
#define _test_monitorableapp_version_h_ 

#include "config/PackageInfo.h"

#define TESTS_TESTMONITORABLEAPP_VERSION_MAJOR 1
#define TESTS_TESTMONITORABLEAPP_VERSION_MINOR 0
#define TESTS_TESTMONITORABLEAPP_VERSION_PATCH 0
#undef TESTS_TESTMONITORABLEAPP_PREVIOUS_VERSIONS  

#define TESTS_TESTMONITORABLEAPP_VERSION_CODE PACKAGE_VERSION_CODE(TESTS_TESTMONITORABLEAPP_VERSION_MAJOR, TESTS_TESTMONITORABLEAPP_VERSION_MINOR, TESTS_TESTMONITORABLEAPP_VERSION_PATCH)
#ifndef TESTS_TESTMONITORABLEAPP_PREVIOUS_VERSIONS
#define TESTS_TESTMONITORABLEAPP_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TESTS_TESTMONITORABLEAPP_VERSION_MAJOR, TESTS_TESTMONITORABLEAPP_VERSION_MINOR, TESTS_TESTMONITORABLEAPP_VERSION_PATCH)
#else 
#define TESTS_TESTMONITORABLEAPP_FULL_VERSION_LIST TESTS_TESTMONITORABLEAPP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TESTS_TESTMONITORABLEAPP_VERSION_MAJOR, TESTS_TESTMONITORABLEAPP_VERSION_MINOR, TESTS_TESTMONITORABLEAPP_VERSION_PATCH)
#endif 


namespace testmonitorableapp 
{
	const std::string project = "tests";
	const std::string package  =  "testmonitorableapp";
	const std::string versions = TESTS_TESTMONITORABLEAPP_FULL_VERSION_LIST;
	const std::string summary = "Simple monitorable xdaq application";
	const std::string description = "Simple monitorable xdaq application for test";
	const std::string authors = "Dainius Simelevicius";
	const std::string link = "https://gitlab.cern.ch/cmsos/tests/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif

