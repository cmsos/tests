import sys
import os
import json
from urllib.request import urlopen
from urllib.request import Request

url = "http://elasticsearch1:9200/cmsos-data-test-master-monitorable/_search?pretty"
d = { "query": { "match_all": {} }, "size": 1, "sort": [ { "creationtime_": { "order": "desc" } } ] }
data = json.dumps(d).encode('utf8')
headers = {'Content-Type': 'application/json'}

print("url: " + url)
print("data: " + json.dumps(d))
print("headers: " + json.dumps(headers))

request = Request(url, data, headers)
response = urlopen(request)
result = response.read().decode()
print("Result:")
print(result)

jsonresult = json.loads(result)

uint64number = json.dumps(jsonresult["hits"]["hits"][0]["_source"]["uint64Field"]).replace('"', '')
expecteduint64number = "18446744073709551615"

assert uint64number == expecteduint64number, "'uint64number' should be %s" % (expecteduint64number)

print("Test passed")
