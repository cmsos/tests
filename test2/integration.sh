#!/usr/bin/env sh

docker_compose_cmd="docker-compose -f test2/docker-compose.yml"

#${docker_compose_cmd} build
# Run service containers, except for test container
${docker_compose_cmd} up -d --no-deps elasticsearch1 elasticsearch2 jobcontrol localbus elastic

# Run the tests
${docker_compose_cmd} run test

# Keep the exit code from tests while we clean up the containers
exit_code=$?

echo "--------- elasticsearch1 logs ---------"
${docker_compose_cmd} logs elasticsearch1
echo "--------- elasticsearch2 logs --------"
${docker_compose_cmd} logs elasticsearch2
echo "--------- jobcontrol logs ------------------"
${docker_compose_cmd} logs jobcontrol
echo "--------- localbus logs ------------------"
${docker_compose_cmd} logs localbus
echo "--------- elastic logs ------------------"
${docker_compose_cmd} logs elastic
echo "--------- test logs -----------------"
${docker_compose_cmd} logs test
echo "--------------------------------------"

# Clean up
${docker_compose_cmd} down

# Return the original result of the test - for GitLab CI
exit ${exit_code}
