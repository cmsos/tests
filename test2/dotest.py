import sys
import os
import json
from urllib.request import urlopen
from urllib.request import Request

url = "http://elasticsearch1:9200/cmsos-data-test-master-jobcontrol/_search?pretty"
d = { "query": { "match_all": {} }, "size": 1, "sort": [ { "creationtime_": { "order": "desc" } } ] }
data = json.dumps(d).encode('utf8')
headers = {'Content-Type': 'application/json'}

print("url: " + url)
print("data: " + json.dumps(d))
print("headers: " + json.dumps(headers))

request = Request(url, data, headers)
response = urlopen(request)
result = response.read().decode()
print("Result:")
print(result)

jsonresult = json.loads(result)

home = json.dumps(jsonresult["hits"]["hits"][0]["_source"]["home"]).replace('"', '')
expectedhome = "/opt/xdaq"

assert home == expectedhome, "'home' should be %s" % (expectedhome)

print("Test passed")
