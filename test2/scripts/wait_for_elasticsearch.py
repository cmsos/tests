import sys
import os
import time
import json
import urllib.request

url = "http://elasticsearch1:9200/_cluster/health?pretty"

pause = 5
for x in range(20):
    for y in range(20):
        try:
            response = urllib.request.urlopen(url)
        except:
            print("Cannot connect to Elasticsearch cluster '%s'. Waiting for %ds..." % (url, pause))
            time.sleep(pause)
            continue
        break
    else:
        print("Time out reached")
        sys.exit(1)

    result = response.read().decode()
    jsonresult = json.loads(result)

    clusterstatus = json.dumps(jsonresult["status"]).replace('"', '')
    expectedclusterstatus = "green"
    print("clusterstatus: %s" % (clusterstatus))
    testcondition = (clusterstatus == expectedclusterstatus)

    if testcondition:
        print("Elasticsearch cluster '%s' is ready" % (url))
        break
    else:
        print("Elasticsearch cluster '%s' is not ready. Waiting for %ds..." % (url, pause))
        time.sleep(pause)
else:
    print("Time out reached")
    sys.exit(1)